# Tango Device Servers #

Bliss is able to simulate both a Tango DatabaseDS and a Tango MySql database where
Yaml configuration is replacing MySql.

List of servers provided by Bliss:

- Axis
- Ct2
- Gpip
- Keithley
- Linkamdsc
- Fuelcell
- Musst
- Nanobpm
- Nanodac
- [Wago](config_tango_wago.md)
