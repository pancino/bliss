# Keithley 428 configuration

Keithley 428 is a Current amplificator

## Yaml sample configuration

```YAML
- class: keithley428
  name: k_bdm26
  gpib_url: enet://gpibd26a
  gpib_pad: 22
```